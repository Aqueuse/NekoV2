# NekoV2

![Neko](https://github.com/Aqueuse/NekoV2/blob/master/src/neko/images/wakeUp.GIF)

NekoV2 is desktop kitty application. Your cat will chase his ball of yarn around the screen,
and he will go to sleep in his basket as soon as he becomes tired.

Validated on Windows 10, with java 15 & 16.

## Installation 

* Download the `.jar` file, and the `run.bat` file on the last release page.

* Be sure to have a Java Development Kit 15 or 16 installed.
  You should be able to find the `java.exe` command on the command line.
  
  => To download the JDK 16 : https://www.oracle.com/java/technologies/javase-jdk16-downloads.html

* Execute the `run.bat` file or double-click the jar file.
